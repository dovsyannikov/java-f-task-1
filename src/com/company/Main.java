package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
    //    1. Дан список смартфонов. Каждый смартфон имеет следующие характеристики: название производителя, диагональ диспле, цена
    //    Необходимо осуществить фильтрацию по производител
    //    (название производителя вводит пользователь), по цене (диапазон цен -
    //    минимум и максимум вводит пользователь), по цене и диагонали дисплея (минимальные
    //    значения для цены и диагонали вводит пользователь).

        Smartphone[] smartphones = Generator.generate();
        //Необходимо осуществить фильтрацию по производител (название производителя вводит пользователь)
        System.out.println("Fill the manufacturer are you interested in: ");
        Scanner scanner = new Scanner(System.in);
        String usersChoiceOfCreator = scanner.nextLine();

        for (Smartphone smartphone : smartphones){
            if (usersChoiceOfCreator.equalsIgnoreCase(smartphone.getCreatorName())){
                Smartphone.print(smartphone);
            }
        }
        System.out.println();
        //по цене (диапазон цен - минимум и максимум вводит пользователь)
        System.out.println("Fill the minimum price for the smartphone");
        float minPrice = Float.parseFloat(scanner.nextLine());
        System.out.println("Fill the maximum price for the smartphone");
        float maxPrice = Float.parseFloat(scanner.nextLine());

        for (Smartphone smartphone : smartphones){
            if (smartphone.getPrice() >= minPrice && smartphone.getPrice()<= maxPrice){
                Smartphone.print(smartphone);
            }
        }
        //по цене и диагонали дисплея (минимальные значения для цены и диагонали вводит пользователь).
        System.out.println();
        System.out.println("Fill the minimum display size of smartphone");
        float displaySize = Float.parseFloat(scanner.nextLine());
        System.out.println("Fill the minium price for the display that size");
        float minPrice2 = Float.parseFloat(scanner.nextLine());

        for (Smartphone smartphone : smartphones){
            if (smartphone.getPrice() >= minPrice2 && smartphone.getScreenSize() >= displaySize){
                Smartphone.print(smartphone);
            }
        }

    }
}
